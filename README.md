# `hh-suite` conda recipe

[![Anaconda-Server Badge](https://anaconda.org/ostrokach-forge/hh-suite/badges/version.svg)](https://anaconda.org/ostrokach-forge/hh-suite)
[![Anaconda-Server Badge](https://anaconda.org/ostrokach-forge/hh-suite/badges/downloads.svg)](https://anaconda.org/ostrokach-forge/hh-suite)

## Environment variables

- **`HHLIB`** - Set automatically by `etc/conda/activate.d/hh-suite.sh` / `etc/conda/deactivate.d/hh-suite.sh`.
- **`PDB_DATA_DIR`** - Path to our local PDB mirror.
- **`DSSP_DATA_DIR`** - Path to our local DSSP mirror.
- **`HHSUITE_SEQUENCE_DB`** -
- **`HHSUITE_STRUCTURE_DB`** -