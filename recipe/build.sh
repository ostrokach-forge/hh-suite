#!/bin/bash

set -ev

MODELLER_VERSION="9.21"
HDF5_VERSION="1820"

# Use `--std=c++14`
export CXXFLAGS=$(echo "${CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')
export DEBUG_CXXFLAGS=$(echo "${DEBUG_CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')

# Dependencies have corrupt libtool files, leading to error:
# `../lib/libiconv.la' is not a valid libtool archive`
# rm -f $PREFIX/lib/*.la

# Make lib symlink

# Set environment variables
cp -f "${RECIPE_DIR}/HHPaths.pm" "${SRC_DIR}/scripts/HHPaths.pm"
sed -i "s|^our \$execdir =$|our \$execdir = \"${PREFIX}/bin\";|" "${SRC_DIR}/scripts/HHPaths.pm"
sed -i "s|^our \$datadir =$|our \$datadir = \"${PREFIX}/data\";|" "${SRC_DIR}/scripts/HHPaths.pm"
sed -i "s|^our \$ncbidir =$|our \$ncbidir = \"${PREFIX}/bin\";|" "${SRC_DIR}/scripts/HHPaths.pm"
sed -i "s|^our \$hhlib =$|our \$hhlib = \"${PREFIX}\";|" "${SRC_DIR}/scripts/HHPaths.pm"
sed -i "s|^our \$hhshare =$|our \$hhshare = \"${PREFIX}\";|" "${SRC_DIR}/scripts/HHPaths.pm"
count=$(grep "${PREFIX}" "${SRC_DIR}/scripts/HHPaths.pm" | wc -l)
if [[ "${count}" != "5" ]] ; then
    echo "Wrong count: ${count}!"
    return -1
fi

# Hack that I don't know how to get rid off
pushd "${PREFIX}"
ln -s lib lib64
popd

# Build hh-suite
mkdir build
cd build
cmake \
    -DCMAKE_INSTALL_PREFIX:PATH="${PREFIX}" \
    -DINCLUDE_INSTALL_DIR:PATH="${PREFIX}/include" \
    -DLIB_INSTALL_DIR:PATH="${PREFIX}/lib" \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -G "Unix Makefiles" \
    ..
make
make install

unlink "${PREFIX}/lib64"

# Make symlinks to scripts in '../scripts' directory so that they are available from "${PATH}"
pushd "${PREFIX}/bin"
for i in ../scripts/*{.py,.pl} ; do
    echo $i ;
    ln -s $i ;
done
popd

# Make symlink to the '../scripts/hhpred/hhpred.pl' file so that it is available from "${PATH}"
pushd "${PREFIX}/bin"
if [[ ! -e ../scripts/hhpred/hhpred.pl ]] ; then
    echo "'hhpred.pl' script must be inside the '../scripts/hhpred' folder!"
    exit -1
fi
ln -s ../scripts/hhpred/hhpred.pl ;
popd

# Make sure that HHLIB environment variable is correctly set when activating environment
mkdir -p ${PREFIX}/etc/conda/activate.d/
echo "export HHLIB=${PREFIX}" > ${PREFIX}/etc/conda/activate.d/hh-suite.sh
mkdir -p ${PREFIX}/etc/conda/deactivate.d/
echo "unset HHLIB" > ${PREFIX}/etc/conda/deactivate.d/hh-suite.sh

# Remove unneccessary files
rm "${PREFIX}/README" "${PREFIX}/LICENSE" "${PREFIX}/hhsuite-userguide.pdf"
